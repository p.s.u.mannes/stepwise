# Stepwise
Playfully organize to-do tasks and challenge yourself
step-by-step.

## Usage
```
python main.py task.example
```
