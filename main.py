#!/usr/bin/python3

import time
import os
import sys
import ast

if len(sys.argv) != 2:
    print("Usage: python main.py task.example")
    sys.exit(1)

input_file = sys.argv[1]

with open(input_file, 'r') as file:
    content = file.read()
    tasks = ast.literal_eval(content)

level=0
total_duration=0

def hustle(level, task):
    os.system('clear')
    print()
    print(f"Level {level}/{len(tasks) - 1}")
    print(task)
    start_time=time.time()
    input("""
        Press enter when:
        - Task is complete
        - Result is checked and good enough
    """)
    end_time=time.time()
    level +=1
    os.system('clear')
    print(f"Good job! You've reached level {level}/{len(tasks)}!")
    duration = end_time - start_time
    input(f"""This challenge took you {round((duration / 60), 2)} minutes.
    Press enter when you are ready for the next challenge.""")
    return (level, duration) 

def welcome_message():
    os.system('clear')
    print(f"""
        Welcome to your challenge.

        Congratulate yourself for taking the first
        step in achieving your goals and making your life better.

        Let's take the small babysteps needed to fulfill your dreams.

    """)
    input("Press ENTER to continue.")

def pray_message():
    os.system('clear')
    print(f"""
        Before we begin, let us pray:

        ~
        God, I praise you with all my heart and soul!
        Hear me in worship as I pray to you.

        Grant me the strength to face your challenges today, so that I may grow
        strong in your image,

        Guide me to learn and discover day-by-day, as I accept the path
        that you have planned for me today.

        Protect me from temptations that mislead me from your path,
        and help me find gratitude in the tasks I face today.

        Grant me the strength and wisdom to forgive others,
        so that I may be forgiven.

        Forever in your glory and your light, may your will
        shine through my soul.

        Amen.
        ~

        """)
    input("Press ENTER to continue.")

def completion_message(start_level, level, max_level):
    challenges_completed = level - start_level

    if challenges_completed == 0:
        print(f"""
        You have completed {challenges_completed} challenges. 
        Try splitting up large challenges into smaller and more manageable steps.
        """)
        return
    elif challenges_completed == 1:
        suffix = ""
    else:
        suffix = "s"

    duration_t = round(total_duration, 2)
    duration_t_min = round(total_duration / 60, 2)
    duration_avg = round(total_duration / challenges_completed, 2)
    duration_avg_min = round(total_duration / challenges_completed / 60, 2)

    if level == max_level:
        print("""
        YOU COMPLETED ALL CHALLENGES!!
        """)

    print(f"""
        Congratulations!

        You have completed {challenges_completed} challenge{suffix}!
        You put in hard work and have grown stronger!
        Give yourself a big pat for showing up today!

        Stats:
        Total duration          | {duration_t} seconds
                                | {duration_t_min} minutes

        Average task duration   | {duration_avg} seconds
                                | {duration_avg_min} minutes

        You have earnt a reward:
        - Take a nap
        - Cook a nice meal
        - Sports
        - Relax
    """)

def get_progress_file_path():
    script_dir = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(script_dir, "progress.txt")

def load_progress():
    progress_file_path = get_progress_file_path()
    if os.path.exists(progress_file_path):
        with open(progress_file_path, "r") as file:
            return int(file.read().strip())
    return None

def save_progress(progress):
    progress_file_path = get_progress_file_path()
    with open(progress_file_path, "w") as file:
        file.write(str(progress))

if __name__ == "__main__":
    welcome_message()
    pray_message()

    try:
        level = load_progress()

        if level is None:
            level = 0

        os.system('clear')
        if level != 0:
            load_choice = input(f"Would you like to resume from where you left off?\nLevel {level} [Y/n]: ").strip().lower()
            if load_choice not in ["yes", "y", ""]:
                level = 0

        start_level = level

        for i, task in enumerate(tasks):
            if level > i:
                continue
            else:
                level, duration = hustle(level, task)
                print(f"Duration: {duration} seconds")
                total_duration += duration

    except KeyboardInterrupt:
        os.system('clear')

        if level > start_level:
            print("\nInterrupted. Saving progress...")
            save_progress(level)
            print(f"Progress saved at level {level}. Exiting...")
            print()

    os.system('clear')
    completion_message(start_level, level, len(tasks))

    # resetting all levels on full completion
    if level == (len(tasks)):
        level = 0
        save_progress(level)



